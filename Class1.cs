﻿using System;
namespace MicHomework
{
    class DataBase
    {
        public List<Student> CreateStudents(int count)
        {
            List<Student> source = new List<Student>(count);
            Random rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                Student student = new Student();
                student.Name = $"A{i + 1}";
                student.Surname = $"A{i + 1}yan";
                student.Email = $"A{i + 1}@gmail.com";
                student.Age = rnd.Next(17, 40);
                student.Phone = $"+374-{rnd.Next(77, 99)}-{rnd.Next(00, 99)}-{rnd.Next(00, 99)}-{rnd.Next(00, 99)}";
                source.Add(student);
            }
            return source;
        }
        public List<Teacher> CreateTeachers(int count)
        {
            List<Teacher> source = new List<Teacher>(count);
            Random rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                Teacher teacher = new Teacher();
                teacher.Name = $"T{i + 1}";
                teacher.Surname = $"T{i + 1}yan";
                teacher.Email = $"T{i + 1}@gmail.com";
                teacher.Age = rnd.Next(17, 40);
                teacher.Rank = rnd.Next(1, 11);
                teacher.Phone = $"+374-{rnd.Next(77, 99)}-{rnd.Next(00, 99)}-{rnd.Next(00, 99)}-{rnd.Next(00, 99)}";
                source.Add(teacher);
            }
            return source;
        }
    }
    class Group
    {
        public string Name { get; set; }
        public Teacher Teacher { get; set; }
        public List<Student> Students { get; set; }
    }
    class MicManager
    {
        public List<Group> CreateGroups(List<Teacher> teachers, List<Student> students)
        {
            var groups = new List<Group>(teachers.Count);


            int toalCount = teachers.Count;
            for (int i = 0; i < teachers.Count; i++)
            {
                Group grup = new Group();
                grup.Name = $"{teachers[i].Name} 's Group";
                grup.Teacher = teachers[i];
                int currentCount = students.Count / toalCount;
                grup.Students = CutFromTop(students, currentCount);
                groups.Add(grup);
                toalCount--;
            }

            return groups;
        }


        static List<Student> CutFromTop(List<Student> source, int count)
        {
            var items = new List<Student>(count);
            while (count > 0)
            {
                Student st = source[0];
                items.Add(st);
                source.RemoveAt(0);
                count--;
            }
            return items;
        }


        static void Shuffle(List<Student> source)
        {
            Random rnd = new Random();
            for (int i = 0; i < source.Count; i++)
            {
                int index = rnd.Next(i, source.Count);
                Swap(source, i, index);
            }
        }
        static void Swap(List<Student> source, int i, int index)
        {
            Student temp = source[i];
            source[i] = source[index];
            source[index] = temp;
        }

    }
    class Person
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public string Phone { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var db = new DataBase();
            List<Teacher> teachers = db.CreateTeachers(3);
            List<Student> students = db.CreateStudents(50);

            var manager = new MicManager();
            List<Group> groups = manager.CreateGroups(teachers, students);

            PrintGroupInfo(groups);
            Console.ReadLine();
        }

        static void PrintGroupInfo(List<Group> groups)
        {
            foreach (var item in groups)
            {
                Console.WriteLine(item.Name);
                Console.WriteLine(item.Students.Count);
            }
        }
    }
    class Student : Person
    {
        public int Course { get; set; }
    }
    class Teacher : Person
    {
        public int Rank { get; set; }
        public string PublicInfo => $"{Name} {Surname}Rank:{Rank} ";
    }
}